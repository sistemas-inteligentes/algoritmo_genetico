import java.util.ArrayList;

public class Cromosoma {
	public ArrayList<Integer> cromosomas;
	private int Numero_Cromosomas;
	private int fitness;
	
	Cromosoma(int Num_Cro){
		this.Numero_Cromosomas=Num_Cro;
		cromosomas = new ArrayList<Integer>(Numero_Cromosomas);
		for (int i = 0; i < Numero_Cromosomas; i++) {
			if (Math.random() > 0.5)
				cromosomas.add(0);
			else
				cromosomas.add(1);
		}
	}
	
	public void mostrar() {
		for (int i = 0; i < Numero_Cromosomas; i++) {
			System.out.print( "[" + cromosomas.get(i) + "]");
		}
		System.out.print( "FITNESS :: " + getFitness());
		System.out.println();
	}
	
	public void calcular_fitnes() {
		fitness=0;
		for (int i = 0; i < Numero_Cromosomas; i++) {
			if (cromosomas.get(i)==1) 
				fitness++;
		}
	}
	
	public int getFitness() {
		return fitness;
	}
	
	public void CrearHijo (Cromosoma Padre,Cromosoma Madre,int NumeroCromosomaPadre ) {
		cromosomas.clear();
		for (int i = 0; i < Numero_Cromosomas; i++) {
			if (i<NumeroCromosomaPadre)
			{
				cromosomas.add(Padre.cromosomas.get(i));
			}
			else
			{
				cromosomas.add(Madre.cromosomas.get(i));
			}
		}
	}
	
	public void Mutar() {
		int num_cromosoma_mutar = (int) (Math.random()*20);
		if (cromosomas.get(num_cromosoma_mutar) == 1)
			cromosomas.set(num_cromosoma_mutar,0);
		else 
			cromosomas.set(num_cromosoma_mutar, 1);
			
	}
	
}


