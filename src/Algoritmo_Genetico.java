import java.util.ArrayList;

public class Algoritmo_Genetico {
	ArrayList<Cromosoma> poblacion = new ArrayList<Cromosoma>();
	ArrayList<Cromosoma> poblacionTemporal = new ArrayList<Cromosoma>();
	//Propio del Programa
	int Numero_Bits = 20;
	int tamanio_poblacion;
	int promedio_fi;
	int promedio_fi_nueva_poblacion;
	//Variables de Evalucaion
	float Probabilidad_Cruce = 0.7f;
	float Probabilidad_Mutacion = 0.001f;
	int NumeroDeGeneraciones;
	
	public Algoritmo_Genetico(int tam) {
		this.tamanio_poblacion = tam;
	}
	
	public void IniciarPoblacion() {
		for (int i = 0; i < tamanio_poblacion; i++) {
			poblacion.add(new Cromosoma(Numero_Bits));
		}
	}
	
	public void Promedio_fi_problacion_actual() {
		promedio_fi=0;
		for (int i = 0; i < poblacion.size(); i++) {
			poblacion.get(i).calcular_fitnes();
			promedio_fi += poblacion.get(i).getFitness();
		}
		promedio_fi=promedio_fi/tamanio_poblacion;
	}
	
	public void mostrar() {
		for (int i = 0; i < tamanio_poblacion; i++) {
			poblacion.get(i).mostrar();
		}
		System.out.println(promedio_fi);
	}


	public void Cruzar_AplicarFitness_Aniadir_Poblacion () 
	{
		//AUXs
		Cromosoma Padre,Madre,hijo1,hijo2;
		
		//DEFINIR QUE SE VAN A CRUZAR
		int Primeros_Bits = (int) (Math.random()*Numero_Bits);
		//CONTROLAR QUE NO SEAN 0 SINO SERIA UN CLON
		if (Primeros_Bits == 0) 
			Primeros_Bits=1;
		
		
		//ASEGURAR QUE SACO UNO MEJOR AL PROMEDIO
		do {
			Padre = poblacion.get((int) (Math.random()*poblacion.size()));
		} while (Padre.getFitness()<promedio_fi);
		do {
			Madre = poblacion.get((int) (Math.random()*poblacion.size()));
		} while (Madre.getFitness()<promedio_fi);
		
		
		//CREAR HIJOS
		hijo1 = new Cromosoma(Numero_Bits);
		hijo1.CrearHijo(Padre, Madre, Primeros_Bits);
		hijo1.calcular_fitnes();
		
		hijo2 = new Cromosoma(Numero_Bits);
		hijo2.CrearHijo(Madre, Padre, Primeros_Bits);
		hijo2.calcular_fitnes();

		//VER SI HAY MUTACIONES
		if (Math.random()<= 0.001)
		{
			hijo1.Mutar();
			hijo2.Mutar();
		}
		poblacionTemporal.add(hijo1);
		poblacionTemporal.add(hijo2);
		
	}

	public void AlgoritmoGenetico() {
		NumeroDeGeneraciones=0;
		IniciarPoblacion();
		while (!converge()) {
			Promedio_fi_problacion_actual();
			for (int j = 0; j < tamanio_poblacion*Probabilidad_Cruce ;j++) {
				Cruzar_AplicarFitness_Aniadir_Poblacion();
			}
			Promedio_fi_nueva_poblacion();
			EliminarCromosomasConBajoFi();
			NuevaPoblacion();
			NumeroDeGeneraciones++;
		}
		System.out.println("Generaciones :: "+ NumeroDeGeneraciones);
	}
	
	//SOLO PARA MONITORIAR
	public void Promedio_fi_nueva_poblacion() {
		int aux=0;
		for (int i = 0; i < poblacionTemporal.size(); i++) {
			poblacionTemporal.get(i).calcular_fitnes();
			aux += poblacionTemporal.get(i).getFitness();
		}
		promedio_fi_nueva_poblacion = aux/poblacionTemporal.size();

	}
	
	public void Mostrar_Nueva_Poblacion ()
	{
		for (int i = 0; i < poblacionTemporal.size(); i++) {
			poblacionTemporal.get(i).mostrar();
		}
	}
	
	public void EliminarPoblacionOriginal() {
		poblacion.clear();
	}
	
	public void EliminarCromosomasConBajoFi() {
		EliminarPoblacionOriginal();
		for (int i = 0; i < poblacionTemporal.size(); i++) {
			if (poblacionTemporal.get(i).getFitness() < promedio_fi_nueva_poblacion) {
				if (poblacionTemporal.size()>tamanio_poblacion) {
					poblacionTemporal.remove(i);	
				}
			}
		}
	}
	
	public void NuevaPoblacion() {
		for (int i = 0; i < tamanio_poblacion; i++) {
			poblacion.add(poblacionTemporal.get(i));
		}Limpiar_Poblacion_Temporal();
	}

	private void Limpiar_Poblacion_Temporal() {
		poblacionTemporal.clear();
	}
	
	public boolean converge() {
		if (promedio_fi >= 19) {
			return true;
		}
		return false;
	}
}

