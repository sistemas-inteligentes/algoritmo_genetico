import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {
		int Numero_De_Pruebas = 20;
		int Promedio = 0;
		

		System.out.println("SISTEMA PROBADO CON :: ");
		System.out.println("0.7 PROB DE CRUCE ");
		System.out.println("0.001 PROB DE MUTACION");
		System.out.println("0.5 PROB DE QUE SEA 1 o 0 PARA EL ESTADO INICIAL ");
		System.out.println("CONVERGENCIA CON UNA PRECISION DEL 95%");
		
		
		ArrayList<Algoritmo_Genetico> pruebas = new ArrayList<Algoritmo_Genetico>();
		//PRUEBAS
		for (int i = 0; i < Numero_De_Pruebas; i++) {
			System.out.print("Prueba # " + (i +1) + " :: ");
			pruebas.add(new Algoritmo_Genetico(100));
			pruebas.get(i).AlgoritmoGenetico();
			Promedio += pruebas.get(i).NumeroDeGeneraciones;
		}
		Promedio = Promedio/Numero_De_Pruebas;
		System.out.println("Promedio :: " + Promedio);
	}
}
